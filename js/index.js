function BT1() {
  var diemchuan = parseInt(document.getElementById("diemchuan").value);
  var khuvuc = document.getElementById("khuvuc").value;
  var doituong = document.getElementById("doituong").value;
  var diem1 = parseInt(document.getElementById("diem1").value);
  var diem2 = parseInt(document.getElementById("diem2").value);
  var diem3 = parseInt(document.getElementById("diem3").value);
  var diemtongket = 0;
  var xepLoai = " ";
  var checkDiem = " ";
  if (diem1 == 0 && diem2 == 0 && diem3 == 0) {
    checkDiem = "Do có điểm nhỏ hơn hoặc bằng 0.";
    document.getElementById("result-bt1").value = "Bạn đã rớt. " + checkDiem;
  }
  switch (khuvuc) {
    case "1":
      khuvuc = 0;
      break;
    case "2":
      khuvuc = 2;
      break;
    case "3":
      khuvuc = 1;
      break;
    case "4":
      khuvuc = 0.5;
      break;
    default:
      break;
  }
  switch (doituong) {
    case "1":
      doituong = 0;
      break;
    case "2":
      doituong = 2.5;
      break;
    case "3":
      doituong = 1.5;
      break;
    case "4":
      doituong = 1;
      break;
    default:
      break;
  }
  diemtongket = khuvuc + doituong + diem1 + diem2 + diem3;
  if ((diemtongket >= diemchuan)) {
    xepLoai = "đậu";
  } else{
    xepLoai = "rớt";
  }
  document.getElementById("result-bt1").value =
    "Bạn đã " + xepLoai + ". Tổng điểm: " + diemtongket;
  return true;
}
function BT2() {
  var name = document.getElementById("name").value;
  var soDien = parseInt(document.getElementById("soDien").value);
  var tienDien = 0;
  let bac1 = 500,
    bac2 = 650,
    bac3 = 850,
    bac4 = 1100,
    bac5 = 1300;
  if (soDien < 0) {
    alert("Vui lòng nhập lại số điện!!! Số điện phải lớn hơn 0.");
    return false;
  } else if (soDien <= 50) {
    tienDien = soDien * bac1;
  } else if (soDien <= 100) {
    tienDien = 50 * bac1 + (soDien - 50) * bac2;
  } else if (soDien <= 200) {
    tienDien = 50 * bac1 + 50 * bac2 + (soDien - 100) * bac3;
  } else if (soDien <= 350) {
    tienDien = 50 * bac1 + 50 * bac2 + 100 * bac3 + (soDien - 200) * bac4;
  } else if (soDien > 350) {
    tienDien =
      50 * bac1 + 50 * bac2 + 100 * bac3 + 100 * bac4 + (soDien - 300) * bac5;
  }
  document.getElementById("tienDien").value =
    "Họ tên: " + name + "; Tiền điện: " + tienDien;
}
function BT3() {
  var username = document.getElementById("username").value;
  var money = parseInt(document.getElementById("money").value);
  var person = parseInt(document.getElementById("person").value);
  var percent = 0;
  var tienthue = money - 4e6 - person * 1.6e6;
  if (money > 0 && money <= 60e6) {
    percent = 0.05;
  } else if (money > 60e6 && money <= 120e6) {
    percent = 0.1;
  } else if (money > 120e6 && money <= 210e6) {
    percent = 0.15;
  } else if (money > 210e6 && money <= 384e6) {
    percent = 0.2;
  } else if (money > 384e6 && money <= 624e6) {
    percent = 0.25;
  } else if (money > 624e6 && money <= 960e6) {
    percent = 0.3;
  } else if (money > 960e6) {
    percent = 0.35;
  }
  document.getElementById("tien-thue-ca-nhan").value =
    "Họ tên: " +
    username +
    "; Tiền thuế thu nhập cá nhân: " +
    tienthue * percent +
    " VND. ";
  return true;
}
function BT4() {
  var khachHang = document.getElementById("khachHang").value;
  var makh = document.getElementById("makh").value;
  // Determine the invoice handling fee based on customer type
  let invoiceHandlingFee = 0;
  if (khachHang === "nhadan") {
    invoiceHandlingFee = 4.5;
  }else if (khachHang === "doanhnghiep") {
    invoiceHandlingFee = 15;
  }
  // Determine the basic service fee based on customer type
  let basicServiceFee = 0;
  if (khachHang === "nhadan") {
    basicServiceFee = 20.5;
  } else if (khachHang === "doanhnghiep") {
    const soketnoi = parseInt(document.getElementById("soketnoi").value);
    basicServiceFee = 75 + (soketnoi - 10) * 5;
  }
  // Determine the premium channel rental based on customer type and channel number
  var soKenh = parseInt(document.getElementById("soKenh").value);
  let premiumChannelRental = 0;
  if (khachHang === "nhadan") {
    premiumChannelRental = 7.5 * soKenh;
  } else if (khachHang === "doanhnghiep") {
    premiumChannelRental = 50 * soKenh;
  }
  // Calculate the total bill and display it on the page
  const totalBill = invoiceHandlingFee + basicServiceFee + premiumChannelRental;
  document.getElementById("result-bt4").value = "Mã khách hàng: " + makh + "; Tiền cáp: $ " + totalBill.toFixed(2);
}